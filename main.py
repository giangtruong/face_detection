import cv2
import numpy as np
import face_detect as fd
import argparse

def main():
    cap = cv2.VideoCapture(0)

    if not cap.isOpened:
        print("Cam had some Error!!")
        exit(-1)

    while(True) :

        ret, frame = cap.read()

        if frame is None:
            print("No Frame")
            exit(-1)

        gray = fd.detect_face(frame)

        cv2.imshow("frame", gray)

        if (cv2.waitKey(1) & 0xFF == ord('q')):
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()

